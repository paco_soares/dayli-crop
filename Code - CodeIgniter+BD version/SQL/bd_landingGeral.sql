-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 28-Out-2019 às 17:39
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dailycrop`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro`
--

CREATE TABLE `cadastro` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `estado` varchar(30) NOT NULL,
  `cidade` varchar(100) NOT NULL,
  `bairro` varchar(100) DEFAULT NULL,
  `rua` varchar(100) DEFAULT NULL,
  `numero` smallint(6) DEFAULT NULL,
  `celular` varchar(20) DEFAULT NULL,
  `produto` varchar(100) NOT NULL,
  `tipo` varchar(50) NOT NULL,
  `descricao` text,
  `imagem` int(11) DEFAULT NULL,
  `data_plantio` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `ciclo`
--

CREATE TABLE `ciclo` (
  `id` int(11) NOT NULL,
  `especie` varchar(20) NOT NULL,
  `imagem` varchar(100) NOT NULL,
  `semana` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `ciclo`
--

INSERT INTO `ciclo` (`id`, `especie`, `imagem`, `semana`) VALUES
(1, 'tomato', 'week1_t2.png', '1'),
(2, 'tomato', 'week2_t2.png', '2'),
(3, 'tomato', 'week2_t2.png', '3'),
(4, 'tomato', 'week3_t2.png', '4'),
(5, 'tomato', 'week3_t2.png', '5'),
(6, 'tomato', 'week4_t2.png', '6'),
(7, 'tomato', 'week4_t2.png', '7'),
(8, 'tomato', 'week5_t2.png', '8'),
(9, 'tomato', 'week5_t2.png', '9'),
(10, 'tomato', 'week5_t2.png', '10'),
(11, 'tomato', 'week2_t6.png', '11'),
(12, 'tomato', 'week2_t6.png', '12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `epoca`
--

CREATE TABLE `epoca` (
  `id` int(11) NOT NULL,
  `mes` int(11) NOT NULL,
  `plantio` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `epoca`
--

INSERT INTO `epoca` (`id`, `mes`, `plantio`) VALUES
(1, 1, 'indicated'),
(2, 2, 'indicated'),
(3, 3, ' most appropriate'),
(4, 4, ' most appropriate'),
(5, 5, ' most appropriate'),
(6, 6, ' most appropriate'),
(7, 7, ' most appropriate'),
(8, 8, ' most appropriate'),
(9, 9, ' most appropriate'),
(10, 10, ' most appropriate'),
(11, 11, 'indicated'),
(12, 12, 'indicated');

-- --------------------------------------------------------

--
-- Estrutura da tabela `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cadastro`
--
ALTER TABLE `cadastro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ciclo`
--
ALTER TABLE `ciclo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `epoca`
--
ALTER TABLE `epoca`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cadastro`
--
ALTER TABLE `cadastro`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ciclo`
--
ALTER TABLE `ciclo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `epoca`
--
ALTER TABLE `epoca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
